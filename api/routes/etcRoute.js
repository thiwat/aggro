'use strict'
const { validateToken } = require('../../utils/auth')
const etcController = require('../controllers/etcController')

module.exports = function(app) {
    app.route('/content')
        .post(validateToken(), etcController.check)
}
