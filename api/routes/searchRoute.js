'use strict'
const { checkSecret, validateToken } = require('../../utils/auth')
const batchesController = require('../controllers/batchesController')
const licensesController = require('../controllers/licensesController')
const reportsController = require('../controllers/reportsController')
const generalController = require('../controllers/generalController')

module.exports = function (app) {
    app.route('/batch')
        .get(validateToken(['staff', 'vip']), batchesController.list)

    app.route('/batch/:batchNo')
        .get(validateToken(['staff', 'vip']), batchesController.get)

    app.route('/v2/license')
        .get(validateToken(['staff', 'vip']), licensesController.list_v2)

    app.route('/license')
        .get(validateToken(['staff', 'vip']), licensesController.list)
        .patch(validateToken(['staff', 'vip']), licensesController.trick_cron)


    app.route('/license/expired_count')
        .get(validateToken(['staff', 'vip']), licensesController.count_expired)

    app.route('/license/expired')
        .get(validateToken(['staff', 'vip']), licensesController.list_expired)

    app.route('/license/:licenseNo')
        .get(validateToken(['staff', 'vip']), licensesController.get)

    app.route('/report/:shopName/year/:year')
        .get(validateToken(['staff', 'vip', 'distributor']), reportsController.get)

    app.route('/report/:shopName/year')
        .get(validateToken(['staff', 'vip', 'distributor']), reportsController.year)

    app.route('/report')
        .get(validateToken(['staff', 'vip', 'distributor']), reportsController.list)

    app.route('/general')
        .get(validateToken(), generalController.list)

    app.route('/general/:itemType/:itemName')
        .get(validateToken(), generalController.getPlant)

    app.route('/general/:id')
        .get(validateToken(), generalController.get)
}