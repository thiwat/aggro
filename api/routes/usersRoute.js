'use strict'
const usersController = require('../controllers/usersController')
const { validateToken } = require('../../utils/auth')

module.exports = function(app) {
    app.route('/login')
        .post(usersController.login)

    app.route('/users')
        .post(usersController.register)
        .get(usersController.check)
        .put(usersController.update)
        .patch(validateToken(), usersController.patch)
    
    app.route('/facebook')
        .post(usersController.facebook)

    app.route('/session/:username')
        .delete(validateToken(['admin']), usersController.deleteSession)
}