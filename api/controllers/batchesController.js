'use strict'
const mongoose = require('mongoose')
const BatchSModel = mongoose.model('batchInfo')
const { logger } = require('../../log')
const { SUCCESS, OUT_OF_SERVICE } = require('../../errors')
const { convertDate } = require('../../utils/date')

exports.list = async function(req, res) {
    const { keyword } = req.query
    try {
        const result = await BatchSModel.distinct('Batch No', {'Batch No': {$regex: keyword.toUpperCase()} })
        return res.json({ ...SUCCESS, result })
    } catch (err) {
        logger('ERR', err)
        return res.status(500).json( OUT_OF_SERVICE )
    }
}

exports.get = async function(req, res) {
    const { batchNo } = req.params

    try {
        const result = await BatchSModel.find({'Batch No': batchNo.toUpperCase() })
        for(const batch of result) {
            batch['วันที่ส่งสินค้า'] = convertDate(batch['วันที่ส่งสินค้า'])
        }
        return res.json({ ...SUCCESS, result })
    } catch (err) {
        logger('ERR', err)
        return res.status(500).json( OUT_OF_SERVICE )
    }
}