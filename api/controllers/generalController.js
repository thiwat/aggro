'use strict'
const mongoose = require('mongoose')
const GeneralModel = mongoose.model('generals')
const { SUCCESS, OUT_OF_SERVICE } = require('../../errors')
const { logger } = require('../../log')
const _ = require('lodash')

const GENERAL_NAME = 'ชื่อสามัญ'
const COMMERCE_NAME = 'ชื่อการค้า'
const SELLER = 'ผู้จัดจำหน่าย'
const AGGRO = 'บริษัท แอ็กโกร (ประเทศไทย) จำกัด'
const SAIMA = 'บริษัท ไซมาเคมี จำกัด'
const PACKING = 'บริษัท แพ็คกิ้ง แอ็ก จำกัด'
const PLANT = 'พืช'
const PEST = 'ศัตรูพืช'

const list = async (req, res) => {
    const { keyword } = req.query
    const regex = new RegExp(`${keyword}`)
    let items
    try {
        items = await GeneralModel.find({
            $or: [
                {[COMMERCE_NAME]: regex},
                {[GENERAL_NAME]: regex}
            ]
        }, {
            _id: 0,
            [COMMERCE_NAME]: 1,
            [GENERAL_NAME]: 1
        })
    } catch (e) {
        logger('ERR', e.message)
        return res.status(500).json( OUT_OF_SERVICE )
    }
    
    const result = {}
    for(const item of items || []) {
        const generalName = item[GENERAL_NAME]
        const commercName = item[COMMERCE_NAME]
        if (generalName.includes(keyword) && !(generalName in result)) {
            result[generalName] = GENERAL_NAME
        }
        if (commercName.includes(keyword) && !(commercName in result)) {
            result[commercName] = COMMERCE_NAME
        }
    }

    return res.json({ ...SUCCESS, result })
}

const getPlant = async (req, res) => {
    const { itemType, itemName } = req.params
    let items
    try {
        items = await GeneralModel.find({ [itemType]: itemName })
    } catch (e) {
        logger('ERR', e.message)
        return res.status(500).json( OUT_OF_SERVICE )
    }

    if(itemType === GENERAL_NAME) {
        const aggroItems = items.filter(item => item[SELLER] === AGGRO)
        const saimaItems = items.filter(item => item[SELLER] === SAIMA)
        const packingItems = items.filter(item => item[SELLER] === PACKING)
        if(!_.isEmpty(aggroItems)) {
            items = aggroItems
        } else if (!_.isEmpty(saimaItems)) {
            items = saimaItems
        } else if (!_.isEmpty(packingItems)) {
            items = packingItems
        } else {
            items = []
        }
    }

    const plants = {}

    for(const item of items) {
        if(item[PLANT] in plants) {
            plants[item[PLANT]][item[PEST]] = item._id
        } else {
            plants[item[PLANT]] = { [item[PEST]] : item._id}
        }
    }

    return res.json({ ...SUCCESS, result: plants })
}

const get = async (req, res) => {
    const { id } = req.params
    const result = await GeneralModel.findOne({ _id: Object(id) })
    return res.json({ ...SUCCESS, ...result._doc })
}

module.exports = { list, getPlant, get }