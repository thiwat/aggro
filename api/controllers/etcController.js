'use strict'
const { SUCCESS } = require('../../errors')
const LogModel = require('../models/logsModel')
const { decodeToken } = require('../../utils/auth')

const check = async (req, res) => {
    const { latitude, longitude } = req.body
    const { username } = decodeToken(req.header('token'))
    const log = new LogModel({
        username,
        latitude,
        longitude,
        timeStamp: Math.floor(new Date() / 1000)
    })
    await log.save()

    res.json({ ...SUCCESS, update: {} })
}

module.exports = { check }