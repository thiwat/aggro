'use strict'

const mongoose = require('mongoose')
const ReportModel = mongoose.model('reports')
const { NO_DATA } = require('../../errors')
const { decodeToken } = require('../../utils/auth')
const { SUCCESS } = require('../../errors')
const _ = require('lodash')

const DATA = 'ข้อมูล'
const MONTH = 'เดือน'
const YEAR = 'ปี'
const SHOP_NAME = 'ชื่อร้านค้า'
const SHOP_CODE = 'รหัสร้านค้า'
const PEST = 'สารกำจัดไรศัตรูพืช'
const BUG = 'สารกำจัดแมลง'
const DISEASE = 'สารกำจัดโรคพืช'
const UNWANTED_FLORA = 'สารกำจัดวัชพืช'
const GROWTH = 'สารควบคุมการเจริญเติบโต'
const SUPPLE_MANTARY = 'อาหารเสริมพืช'
const PERFORMANCE = 'สารเพิ่มประสิทธิภาพ'
const TOTAL = 'มูลค่ารวม (บาท)'

const REPORT_FIXED_SIZE = 0
const MINIMUN_SHOW_LABEL = 10

const month = ['ม.ค', 'ก.พ.', 'มี.ค.', 'เม.ย.', 'พ.ค.', 'มิ.ย.', 'ก.ค.', 'ส.ค.', 'ก.ย.', 'ต.ค.', 'พ.ย.', 'ธ.ค.']
const tableHead = ['', PEST, SUPPLE_MANTARY, DISEASE, 'สารควบคุม การเจริญเติบโต', UNWANTED_FLORA, BUG, PERFORMANCE, TOTAL]

const get = async (req, res) => {
    const { shopName, year } = req.params
    const { shops, role } = decodeToken(req.header('token'))

    const result = await ReportModel.find({ [SHOP_NAME]: shopName, [YEAR]: parseInt(year) }).sort({ [MONTH]: 1 })

    if (_.isEmpty(result)) {
        return res.status(404).json(NO_DATA)
    }
    if (role === 'staff' && !shops.includes(result[0][[SHOP_CODE]].split('-')[0])) {
        return res.status(400).json(NO_DATA)
    }

    const reports = []
    const pieData = {}
    const barData = []
    const total = ['รวม', 0, 0, 0, 0, 0, 0, 0, 0]
    const headers = [PEST, SUPPLE_MANTARY, DISEASE, GROWTH, UNWANTED_FLORA, BUG, PERFORMANCE, TOTAL]
    for (let i = 0; i < month.length; i++) {
        const report = result.find(item => item[MONTH] === i + 1)
        if (report) {
            reports.push(
                [
                    month[i],
                    report[DATA][PEST].toFixed(REPORT_FIXED_SIZE),
                    report[DATA][SUPPLE_MANTARY].toFixed(REPORT_FIXED_SIZE),
                    report[DATA][DISEASE].toFixed(REPORT_FIXED_SIZE),
                    report[DATA][GROWTH].toFixed(REPORT_FIXED_SIZE),
                    report[DATA][UNWANTED_FLORA].toFixed(REPORT_FIXED_SIZE),
                    report[DATA][BUG].toFixed(REPORT_FIXED_SIZE),
                    report[DATA][PERFORMANCE].toFixed(REPORT_FIXED_SIZE),
                    report[DATA][TOTAL].toFixed(REPORT_FIXED_SIZE)
                ]
            )
            for (let i = 0; i < headers.length; i++) {
                total[i + 1] += Math.floor(report[DATA][headers[i]])
            }
            barData.push(Math.floor(report[DATA][TOTAL]))
        } else {
            reports.push(
                [
                    month[i], 0, 0, 0, 0, 0, 0, 0, 0
                ]
            )
            barData.push(0)
        }
    }

    for (let i = 0; i < headers.length - 1; i++) {
        const value = total[i + 1]
        const percent = ((total[i + 1] / total[8]) * 100).toFixed(1)
        pieData[headers[i]] = {
            value,
            percent: percent > MINIMUN_SHOW_LABEL ? `${percent}%` : ''
        }
    }

    reports.push(total)

    return res.json({
        ...SUCCESS,
        name: shopName,
        id: result[0][[SHOP_CODE]],
        head: tableHead,
        reports,
        pieData,
        barData
    })
}

const list = async (req, res) => {
    const { shops, role } = decodeToken(req.header('token'))
    for (let i = 0; i < shops.length; i++) {
        shops[i] = new RegExp(`^${shops[i]}`)
    }
    const query = role === 'vip' ? {} : { [SHOP_CODE]: { $in: shops } }
    const result = await ReportModel.distinct(SHOP_NAME, query)

    return res.json({ ...SUCCESS, result })
}

const year = async (req, res) => {
    const { shopName } = req.params
    const result = await ReportModel.distinct(YEAR, { [SHOP_NAME]: shopName })
    return res.json({ ...SUCCESS, result })
}

module.exports = { get, list, year }