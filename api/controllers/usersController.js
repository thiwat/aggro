'use strict'
const mongoose = require('mongoose')
const { genHash } = require('../../utils/hash')
const UsersModel = mongoose.model('users')
const FacebookModel = mongoose.model('facebooks')
const { logger } = require('../../log')
const { setCache, setExpire, delCache } = require('../../utils/cache')
const { generateToken, decodeToken } = require('../../utils/auth')
const { loginValidate, registerValidate } = require('../../utils/validate')
const { SUCCESS, OUT_OF_SERVICE, NO_USER, WRONG_UNIQUE, WRONG_PASSWORD, DUPLICATE_USER } = require('../../errors')
const _ = require('lodash')

const EXPIRE_TIME = 14 * 24 * 60 * 60

const login = async (req, res) => {
    const { username, password, uniqueId } = req.body
    if(username !== 'admin') {
        const validate = loginValidate(username, password)
        if(!_.isUndefined(validate)) {
            return res.status(400).json( validate )
        }
    }

    let user
    try {
        user = await UsersModel.findOne({ username })
    } catch (err) {
        logger('ERR', err)
        return res.status(500).json( OUT_OF_SERVICE )
    }

    if (!user) {
        logger('INFO', `${username} not exist`)
        return res.status(404).json( NO_USER )
    }

    if(user.uniqueId !== undefined && user.uniqueId != uniqueId) {
        logger('INFO', `${username} login with wrong unique id`)
        return res.status(401).json( WRONG_UNIQUE )
    }

    if(genHash(`${password}${user.createDate}`) !== user.password) {
        logger('INFO', `${username} login with wrong password`)
        return res.status(401).json( WRONG_PASSWORD )
    }

    if(user.uniqueId === undefined) {
        await UsersModel.updateOne({ username }, { $set: { uniqueId }})
    }

    const token = generateToken({username, name: user.name, role: user.role, shops: user.shops, company: user.company })
    const secret = genHash(token)
    const refreshToken = genHash(token)
    const returnToken = `${token}:${secret}`
    try {
        await setCache(refreshToken, "1")
        await setExpire(refreshToken, EXPIRE_TIME)
    } catch (err) {
        return res.status(500).json('try in later')
    }

    res.json({ ...SUCCESS, returnToken })
}

const register = async (req, res) => {
    const { username, password, name, role, staffId, company, shops } = req.body
    const validate = registerValidate(username, password, name)
    if(!_.isUndefined(validate)) {
        return res.status(400).json( validate )
    }

    try {
        const users = await UsersModel.countDocuments({ username })
        if (users) {
            return res.status(400).json( DUPLICATE_USER )
        }
    } catch (err) {
        logger('ERR', err)
        return res.status(500).json( OUT_OF_SERVICE )
    }

    const createDate = Math.floor(new Date())
    const storePassword = genHash(`${password}${createDate}`)

    try {
        const user = new UsersModel({ username, password: storePassword, name, role, staffId, company, shops, createDate })
        await user.save()
        logger('INFO', `${username} register success`)
    } catch (err) {
        logger('ERR', err)
        return res.status(500).json( OUT_OF_SERVICE )
    }

    res.status(201).json( SUCCESS )
}

const deleteSession = async (req, res) => {
    const { username } = req.params

    try {
        const user = await UsersModel.findOne({ username })
        const token = generateToken({username, name: user.name, role: user.role, shops: user.shops, company: user.company })
        const refreshToken = genHash(token)
        await delCache(refreshToken)
        logger('INFO', `delete ${username} session success`)
        return res.json( SUCCESS )
    } catch (e) {
        logger('ERR', e.message)
        return res.status(500).json( OUT_OF_SERVICE )
    }
}

const check = async (req, res) => {
    const { username, uniqueId } = req.query

    const user = await UsersModel.findOne({ username })

    if (_.isNull(user)) {
        return res.status(404).json( NO_USER )
    }

    if(user.uniqueId !== undefined && user.uniqueId != uniqueId) {
        logger('INFO', `${username} check with wrong unique id`)
        return res.status(401).json( WRONG_UNIQUE )
    }

    return res.json( SUCCESS )
}

const update = async (req, res) => {
    const { username, password, uniqueId } = req.body

    const user = await UsersModel.findOne({ username })
    const storePassword = genHash(`${password}${user.createDate}`)

    if(user.uniqueId !== undefined && user.uniqueId != uniqueId) {
        logger('INFO', `${username} check with wrong unique id`)
        return res.status(401).json( WRONG_UNIQUE )
    }

    try {
        await UsersModel.updateOne({ username }, {$set: { password: storePassword }})
    } catch (e) {
        logger('ERR', e.message)
        return res.status(500).json( OUT_OF_SERVICE )
    }
    return res.json( SUCCESS )
}

const patch = async (req, res) => {
    const { password, newPassword, uniqueId } = req.body
    const { username } = decodeToken(req.header('token'))

    const user = await UsersModel.findOne({ username })

    if(user.uniqueId !== undefined && user.uniqueId != uniqueId) {
        logger('INFO', `${username} change password with wrong unique id`)
        return res.status(401).json( WRONG_UNIQUE )
    }

    if(genHash(`${password}${user.createDate}`) !== user.password) {
        logger('INFO', `${username} change password with wrong password`)
        return res.status(401).json( WRONG_PASSWORD )
    }

    const storePassword = genHash(`${newPassword}${user.createDate}`)

    try {
        await UsersModel.updateOne({ username }, {$set: {password: storePassword }})
    } catch (e) {
        logger('ERR', e.message)
        return res.status(500).json( OUT_OF_SERVICE )
    }

    return res.json( SUCCESS )
}

const facebook = async (req, res) => {
    const { id, name, uniqueId } = req.body
    let user = await FacebookModel.findOne({ id })
    if(_.isNull(user)) {
        const newUser = new FacebookModel({
            id,
            name,
            role: 'general',
            uniqueId,
            createDate: Math.floor(new Date() / 1000)
        })
        user = await newUser.save()
    }
    const token = generateToken({username: id, name, role: user.role })
    const secret = genHash(token)
    const refreshToken = genHash(token)
    const returnToken = `${token}:${secret}`
    try {
        await setCache(refreshToken, "1")
        await setExpire(refreshToken, EXPIRE_TIME)
    } catch (err) {
        return res.status(500).json('try in later')
    }

    return res.json({ ...SUCCESS, returnToken })
}

module.exports = { login, register, deleteSession, check, update, patch, facebook }