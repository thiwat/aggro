'use strict'
const mongoose = require('mongoose')
const LicensesModel = mongoose.model('licenseInfo')
const { logger } = require('../../log')
const { SUCCESS, OUT_OF_SERVICE } = require('../../errors')
const { convertDate } = require('../../utils/date')
const _ = require('lodash')
const moment = require('moment')

const ONE_DAY = 60 * 60 * 24
const ONE_YEAR = 365
const TWO_MONTH = 2 * 30
const TWO_MONTH_LIST = ['วันที่หมดอายุใบอนุญาตนำเข้า', 'วันหมดอายุใบอนุญาตผลิต']
const DATE_LIST = ['วันที่ได้รับทะเบียน', 'วันที่หมดอายุใบอนุญาตนำเข้า', 'วันที่ได้ทะเบียนผลิต', 'วันหมดอายุทะเบียนผลิต', 'วันที่ได้ใบอนุญาตผลิต', 'วันหมดอายุใบอนุญาตผลิต', 'วันที่ครอบครอง', 'วันที่หมดอายุทะเบียนนำเข้า', 'วันทีได้รับใบอนุญาตนำเข้า']
const EXPIRED_DATE_LIST = ['วันที่หมดอายุใบอนุญาตนำเข้า', 'วันหมดอายุทะเบียนผลิต', 'วันหมดอายุใบอนุญาตผลิต', 'วันที่หมดอายุทะเบียนนำเข้า']

exports.list_v2 = async function (req, res) {
  try {
    const { keyword } = req.query

    const licenses = await LicensesModel.find({
      $and: [
        { 'ทะเบียนผลิต': { $exists: true } },
        { 'ทะเบียนผลิต': { $ne: '-' } },
      ],
      $or: [
        { 'ทะเบียนผลิต': { $regex: keyword, $options: 'i' } },
        { 'ชื่อสามัญ': { $regex: keyword, $options: 'i' } },
        { 'ชื่อการค้า': { $regex: keyword, $options: 'i' } },
      ]
    })

    const result = {}
    for (const license of licenses) {
      const isLicense = license['ทะเบียนผลิต'].includes(keyword)
      const key = isLicense
        ? license['ทะเบียนผลิต'].split('-')[0]
        : license['ชื่อการค้า'].includes(keyword) ? license['ชื่อการค้า'] : license['ชื่อสามัญ']
      const choice = isLicense
        ? license['ทะเบียนผลิต'].split('-')[1]
        : license['ทะเบียนผลิต']
      if (result[key]) {
        result[key]['choices'].push(choice)
      } else {
        result[key] = {
          type: isLicense ? 'license' : 'name',
          choices: [choice]
        }
      }
    }

    res.json({ ...SUCCESS, result })
  } catch (e) {
    logger('ERR', e)
    return res.status(500).json(OUT_OF_SERVICE)
  }
}


exports.list = async function (req, res) {
  const { keyword } = req.query
  const regex = new RegExp(`^${keyword}[0-9]{0,2}-`)
  let result

  try {
    result = await LicensesModel.distinct('ทะเบียนผลิต', { 'ทะเบียนผลิต': regex })
  } catch (err) {
    logger('ERR', err)
    return res.status(500).json(OUT_OF_SERVICE)
  }

  let licenseList = {}
  for (let i = 0, max = result.length; i < max; i++) {
    let [licenseNo, year] = result[i].split('-')
    let checkExist = licenseNo in licenseList
    if (checkExist) {
      licenseList[licenseNo] = [...licenseList[licenseNo], year]
    } else {
      licenseList[licenseNo] = [year]
    }
  }

  res.json({ ...SUCCESS, result: licenseList })
}

exports.get = async function (req, res) {
  const { licenseNo } = req.params

  try {
    const result = await LicensesModel.find({ 'ทะเบียนผลิต': licenseNo }, { _id: 0 })
    for (const license of result) {
      for (const key of DATE_LIST) {
        license[key] = convertDate(license[key])
      }
    }
    res.json({ ...SUCCESS, result })
  } catch (err) {
    logger('ERR', err)
    return res.status(500).json(OUT_OF_SERVICE)
  }
}

exports.list_expired = async (req, res) => {
  try {
    const result = await LicensesModel.find({
      have_expired: true
    })
    res.json({
      ...SUCCESS,
      result: result.map(item => {
        for (const key of [...item.expired, ...item.near_expire]) {
          item[key] = convertDate(item[key])
        }
        return item
      })
    })
  } catch (e) {
    logger('ERR', e)
    return res.status(500).json(OUT_OF_SERVICE)
  }
}

exports.count_expired = async (req, res) => {
  try {
    const result = await LicensesModel.countDocuments({
      have_expired: true
    })
    res.json({
      ...SUCCESS,
      result
    })
  } catch (e) {
    logger('ERR', e)
    return res.status(500).json(OUT_OF_SERVICE)
  }
}

exports.trick_cron = function (req, res) {
  cron()
  res.json({ ...SUCCESS })
}

const cron = async () => {
  try {
    logger('INFO', 'Start checking expired date')
    const licenses = await LicensesModel.find()
    const today = moment().startOf('day').unix()
    for (const license of licenses) {
      const expired = [], near_expire = []
      for (const key of EXPIRED_DATE_LIST) {
        let date = license[key]
        const temp = new Date(date || '-')
        if (!_.isNaN(temp.getDate())) {
          const temp2 = moment(temp).endOf('day').unix()
          const diff = ((temp2 - today) / ONE_DAY) - 1
          const NEAR_EXPIRED_THRESHOLD = TWO_MONTH_LIST.includes(key)
            ? TWO_MONTH
            : ONE_YEAR
          if (diff < 0) {
            expired.push(key)
          } else if (diff < NEAR_EXPIRED_THRESHOLD) {
            near_expire.push(key)
          }
        }
      }
      await LicensesModel.updateOne(
        {
          _id: license._id
        },
        {
          $set: {
            expired,
            near_expire,
            have_expired: expired.length > 0 || near_expire.length > 0,
          }
        }
      )
    }
    logger('INFO', 'Check expired date successfully.')
  } catch (e) {
    logger('ERR', e)
  }
}

exports.cron = cron