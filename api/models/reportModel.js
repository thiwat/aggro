const mongoose = require('mongoose')
const Schema = mongoose.Schema

const reportsSchema = new Schema ({
    'รหัสร้านค้า': String,
    'ชื่อร้านค้า': String,
    'เดือน': Number,
    'บริษัท': String,
    'ข้อมูล': Object,
    'ปี': Number
})

module.exports = mongoose.model('reports', reportsSchema, 'statsInfos')