'use strict'
var mongoose = require('mongoose')
var Schema = mongoose.Schema

var generalSchema = new Schema ({
    "ประเภทสาร": {
        type: String
    },
    "ผู้จัดจำหน่าย": {
        type: String
    },
    "ชื่อสามัญ": {
        type: String
    },
    "เปอร์เซ็นต์สาร": {
        type: String
    },
    "สูตร": {
        type: String
    },
    "ชื่อการค้า": {
        type: String
    },
    "ศัตรูพืช": {
        type: String
    },
    "พืช": {
        type: String
    },
    "อัตราการใช้": {
        type: String
    },
    "กลุ่มสารเคมี": {
        type: mongoose.Schema.Types.Mixed
    },
    "กลไกการออกฤทธิ์": {
        type: String
    }
})

module.exports = mongoose.model("generals", generalSchema, "generalInfos")