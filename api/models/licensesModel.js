'use strict'
const mongoose = require('mongoose')
const Schema = mongoose.Schema

const licenseSchema = new Schema({
    'ชื่อสามัญ': String,
    'อัตราส่วน': String,
    'ชื่อการค้า': String,
    'แหล่งนำเข้า': String,
    'ผู้นำเข้า': String,
    'ผู้ผลิต': String,
    'ผู้จัดจำหน่าย(ที่ระบุบนฉลาก)': String,
    'ประเภทการผลิต': String,
    'ประเภทผลิตภัณฑ์': String,
    'ทะเบียนนำเข้า': String,
    'วันที่ได้รับทะเบียน': String,
    'วันที่หมดอายุ': String,
    'ใบอนุญาตนำเข้า': String,
    'วันทีได้รับใบอนุญาตนำเข้า': String,
    'วันที่หมดอายุทะเบียนนำเข้า': String,
    'วันที่หมดอายุใบอนุญาตนำเข้า': String,
    'ทะเบียนผลิต': String,
    'วันที่ได้รับทะเบียนผลิต': String,
    'วันที่ได้ทะเบียนผลิต': String,
    'วันหมดอายุทะเบียนผลิต': String,
    'ใบอนุญาตผลิต': String,
    'วันที่ได้ใบอนุญาตผลิต': String,
    'วันหมดอายุใบอนุญาตผลิต': String,
    'ใบอนุญาตครอบครอง': String,
    'ลำดับใบอนุญาตครอบครอง': Number,
    'วันที่ครอบครอง': String,
    "expired": Array,
    "near_expire": Array,
    "have_expired": Boolean
})

module.exports = mongoose.model("licenseInfo", licenseSchema, "licenseInfos")