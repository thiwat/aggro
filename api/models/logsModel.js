const mongoose = require('mongoose')
const Schema = mongoose.Schema

const logModel = new Schema ({
    username: String,
    latitude: Number,
    longitude: Number,
    timeStamp: Number
})

module.exports = mongoose.model('logs', logModel)