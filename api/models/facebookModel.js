'use strict'
const mongoose = require('mongoose')
const Schema = mongoose.Schema

const facebookSchema = new Schema ({
    id: String,
    name: String,
    role: String,
    uniqueId: String,
    createDate: Number,
    updateDate: Number
})

module.exports = mongoose.model('facebooks', facebookSchema)