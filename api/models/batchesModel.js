'use strict'
const mongoose = require('mongoose')
const Schema = mongoose.Schema

const batchsSchema = new Schema ({
    'วันที่ส่งสินค้า': String,
    'เลขที่ใบส่งสินค้า': String,
    'รหัสลูกค้า': String,
    'ชื่อลูกค้า': String,
    'ที่อยู่ลูกค้า': String,
    'ช่อสินค้า': String,
    'ขนาด': String,
    'หน่วย': String,
    'จำนวน': Number,
    'Lot No': String,
    'Batch No': String
})

module.exports = mongoose.model('batchInfo', batchsSchema, 'batchInfos')