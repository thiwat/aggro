const mongoose = require('mongoose')
const Schema = mongoose.Schema

const usersSchema = new Schema ({
    username: String,
    password: String,
    name: String,
    role: String,
    company: String,
    staffId: String,
    shops: [String],
    uniqueId: String,
    createDate: Number,
    updateDate: Number
})

module.exports = mongoose.model('users', usersSchema)