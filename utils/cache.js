const redis = require('redis')
const client = redis.createClient({ host: process.env.REDIS_URL })
const { promisify } = require('util')
const { logger } = require('../log')

const PING_INTERVAL = 5 * 60 * 1000 // 5 minutes

const pingInterval = async () => {
    if (await ping() !== "PONG") {
        logger('REDIS', 'Redis connection is down')
    }
    setTimeout(pingInterval, PING_INTERVAL)
}

client.on('connect', pingInterval)

client.on('error', function(err) {
    logger('ERR', err)
})

exports.getCache = promisify(client.get).bind(client)

exports.setCache = promisify(client.set).bind(client)

exports.setExpire = promisify(client.expire).bind(client)

exports.delCache = promisify(client.del).bind(client)

const ping = promisify(client.ping).bind(client)
