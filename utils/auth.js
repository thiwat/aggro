'use strict'

const { logger } = require('../log')
const { WRONG_KEY, INVALID_TOKEN, TOKEN_EXPIRED, NO_PERMISSION } = require('../errors')
const { genHash } = require('../utils/hash')
const { getCache, setExpire } = require('../utils/cache')

const EXPIRE_TIME = 14 * 24 * 60 * 60

const checkSecret = (req, res, next) => {
    const secretKey = req.header('secretKey')
    const { remoteAddress } = req.connection
    if ( secretKey != process.env.SECRET_KEY ) {
        logger('AUTH', `${remoteAddress} access with wrong secret key (${secretKey})`)
        return res.status(401).json(WRONG_KEY)
    }
    next()
}

const validateToken = (roles=['staff', 'distributor', 'general', 'vip', 'admin']) => {
    return async function(req, res, next) {
        const token = req.header('token')
        const { remoteAddress } = req.connection

        if (!token) {
            return res.status(400).json({ message: "Token not found"})
        }

        const key = token.split(':')[0]
        const refreshToken = genHash(key)

        const result = await getCache(refreshToken)

        if (!result) {
            logger('AUTH', `${remoteAddress} access with expired token`)
            return res.status(401).json( TOKEN_EXPIRED )
        }

        if(!checkToken(token)) {
            logger('AUTH', `${remoteAddress} access with invalid token`)
            return res.status(401).json( INVALID_TOKEN )
        }
        
        const { role, username } = decodeToken(token)

        if(!roles.includes(role)) {
            logger('AUTH', `${username} try to access without permission`)
            return res.status(401).json( NO_PERMISSION )
        }

        await setExpire(refreshToken, EXPIRE_TIME)

        next()
    }
}

const generateToken = (data) => {
    const bodyToken = new Buffer.from(JSON.stringify(data)).toString('base64')
    return bodyToken
}

const decodeToken = (token) => {
    const body = token.split(':')[0]
    const data = new Buffer.from(body, 'base64')
    return JSON.parse(data)
}

const checkToken = (token) => {
    let message, secret
    try {
        [message, secret] = token.split(':')
    } catch (err) {
        return false
    }
    if(genHash(message) != secret ) {
        return false
    }

    return true
}


module.exports = { checkSecret, generateToken, decodeToken, validateToken }