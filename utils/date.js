'use strict'
const _ = require('lodash')

const convertDate = (time_stamp) => {
  try {
    let date = new Date(time_stamp)
    if (_.isNaN(date.getDate())) {
      throw new Error('Invalid Date')
    }
    date = new Date(date.setDate(date.getDate() + 1))
    return `${_pad(date.getDate())} / ${_pad(date.getMonth() + 1)} / ${_checkYear(date.getFullYear())}`
  } catch (e) {
    return time_stamp
  }
}

const _pad = (value) => {
  return ("0" + value.toString()).slice(-2)
}

const _checkYear = (value) => {
  if (value < 2200) {
    value += 543
  }
  return value
}

module.exports = { convertDate }
