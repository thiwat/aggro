'use strict'

const { readFileSync } = require('fs')

const readImage = async (path) => {
  return await readFileSync(path, 'base64')
}

module.exports = { readImage }