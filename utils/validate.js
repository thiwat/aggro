'use strict'
const { USERNAME_INVALID, PASSWORD_INVALID, NAME_INVALID } = require('../errors')

const validate = (function() {
    const _passwordFormat = /^[a-zA-Z0-9]{6,20}$/
    const _emailFormat = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    const _phoneNumberFormat = /^0[0-9]{9}$/
    const _nameFormat = /^[a-zA-Zก-๑]+ [a-zA-Zก-๑]+/

    const _isPassword = (password) => {
        return !!password.match(_passwordFormat)
    }
    const _isEmail = (email) => {
        return !!email.match(_emailFormat)
    }
    const _isPhoneNumber = (phoneNumber) => {
        return !!phoneNumber.match(_phoneNumberFormat)
    }

    const _isName = (name) => {
        return !!name.match(_nameFormat)
    }

    const loginValidate = (username, password) => {
        if(!_isPhoneNumber(username) && !_isEmail(username)) {
            return USERNAME_INVALID
        }
        if(!_isPassword(password)) {
            return PASSWORD_INVALID
        }
    }

    const registerValidate = (username, password, name) => {
        if(!_isPhoneNumber(username) && !_isEmail(username)) {
            return USERNAME_INVALID
        }
        if(!_isPassword(password)) {
            return PASSWORD_INVALID
        }
        if(!_isName(name)) {
            return NAME_INVALID
        }
    }

    return {
        loginValidate,
        registerValidate
    }
})()

module.exports = validate