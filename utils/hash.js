'use strict'
const { createHmac } = require('crypto')

exports.genHash = function(message) {
    return createHmac('sha256', process.env.SALT).update(message).digest('hex')
}