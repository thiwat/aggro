exports.SUCCESS = new ReturnMessage(0, "Success")

exports.INVALID_TOKEN = new ReturnMessage(1, "เซสชั่นไม่ถูกต้อบ")

exports.WRONG_KEY = new ReturnMessage(2, "โปรดอัพเดทเวอชั่น")

exports.OUT_OF_SERVICE = new ReturnMessage(3, "กรุณาลองใหม่ภายหลัง")

exports.NO_USER = new ReturnMessage(4, "ไม่พบผู้ใช้นี้ในระบบ")

exports.WRONG_UNIQUE = new ReturnMessage(5, "โปรดใช้เครื่องเดิมในการใช้การงาน")

exports.WRONG_PASSWORD = new ReturnMessage(6, "รหัสผ่านไม่ถูกต้อง")

exports.DUPLICATE_USER = new ReturnMessage(7, "อีเมลล์หรือเบอร์โทรนี้ถูกใช้งานแล้ว")

exports.NO_PERMISSION = new ReturnMessage(8, "ไม่มีสิทธิ์ใช้งาน")

exports.NO_DATA = new ReturnMessage(9, "ไม่พบข้อมูล")

exports.SESSION_EXPIRED = new ReturnMessage(10, "เซสชั่นหมดอายุ")

exports.TOKEN_EXPIRED = new ReturnMessage(11, "เซสชั่นหมดอายุ")

exports.USERNAME_INVALID = new ReturnMessage(12, "โปรดกรอกอีเมลล์หรืิอเบอร์โทรให้ถูกต้อง")

exports.PASSWORD_INVALID = new ReturnMessage(13, "รหัสผ่าน 6-20 ตัว ห้ามมีอักขระพิเศษ")

exports.NAME_INVALID = new ReturnMessage(14, "โปรดใส่ชื่อ นามสกุล ให้ถูกต้อง")

function ReturnMessage(code, message) {
    this.code = code
    this.message = message
}