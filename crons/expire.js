const cron = require('node-cron')
const { logger } = require('../log')
const controller = require('../api/controllers/licensesController')


cron.schedule('0 0 * * *', () => {
  logger('CRON', 'Check license expired cron job is running')
  controller.cron()
}, {
    timezone: 'Asia/Bangkok'
  }
)