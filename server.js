require('dotenv').config()
const express = require('express')
const bodyParser = require('body-parser')
const mongoose = require('mongoose')
const cors = require('cors')
const { logger, requestLogger } = require('./log')
const { checkSecret } = require('./utils/auth')


const app = express()
const port = process.env.PORT || 3000

// initial mongo model
require('./api/models/usersModel')
require('./api/models/batchesModel')
require('./api/models/licensesModel')
require('./api/models/reportModel')
require('./api/models/generalModel')
require('./api/models/logsModel')
require('./api/models/facebookModel')

// cron job
require('./crons/expire')


// graceful shutdown
function gracefulShutdown() {
    logger('INFO', 'Got terminate signal, Server is shutting down..')
    server.close(() => {
        logger('INFO', 'Shutdown server success')
        process.exit()
    })

    const timeout = 10 * 1000 // 10 seconds
    setTimeout(() => {
        logger('ERR', 'Can not shutdown server, Force shutdown the server')
        process.exit(1)
    }, timeout)
}

process.on('SIGTERM', gracefulShutdown)
process.on('SIGINT', gracefulShutdown)

// mongo db connection
mongoose.connect(process.env.MONGODB_URL, { useNewUrlParser: true })
const db = mongoose.connection
db.on('error', console.error.bind(console, 'connection error : '))

// for cors
app.use(cors())

// for json body
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

// for request logger
app.use(requestLogger)
app.use(checkSecret)

// api routes
const usersRoutes = require('./api/routes/usersRoute')
const searchRoutes = require('./api/routes/searchRoute')
const etcRoutes = require('./api/routes/etcRoute')
usersRoutes(app)
searchRoutes(app)
etcRoutes(app)

let server = app.listen(port)
logger('INFO', `Server is ready for listen and serve with port ${port}`)

module.exports = app
